# CLI connection
# docker run --network host --rm -it mariadb:11.4.2-ubi9 mariadb -u root -h 127.0.0.1 --port 3306

USE reservasi_hotel;

# Tampilkan tipe kamar dan harga malam
SELECT tipe_kamar, harga_malam FROM tb_kamar;

# Tampilkan nama customer yang check in tanggal [bebas]
SELECT tb_pelanggan.name FROM tb_reservasi RIGHT JOIN tb_pelanggan ON tb_reservasi.customer_id = tb_pelanggan.customer_id
    WHERE tanggal_checkin = '2024-07-29';

# Tampilkan nama customer yang check out pada tanggal
SELECT tb_pelanggan.name FROM tb_reservasi RIGHT JOIN tb_pelanggan ON tb_reservasi.customer_id = tb_pelanggan.customer_id
    WHERE tanggal_checkout = '2024-07-17';

# Tampilkan nama customer, nomer telepon, alamat yang check in tanggal
SELECT tb_pelanggan.name, tb_pelanggan.no_telepon, tb_pelanggan.alamat FROM tb_reservasi RIGHT JOIN tb_pelanggan ON tb_reservasi.customer_id = tb_pelanggan.customer_id
    WHERE tanggal_checkin = '2024-07-29';

# Tampilkan nama customer, yang jumlah pembayarannya lebih dari 1jt
SELECT tb_pelanggan.name FROM tb_reservasi RIGHT JOIN tb_pembayaran ON tb_pembayaran.payment_id = tb_reservasi.reservasi_id
    LEFT JOIN tb_pelanggan ON tb_reservasi.customer_id = tb_pelanggan.customer_id
    WHERE jumlah_pembayaran > 1000000;
