# CLI connection
# docker run --network host --rm -it mariadb:11.4.2-ubi9 mariadb -u root -h 127.0.0.1 --port 3306

CREATE DATABASE reservasi_hotel;

USE reservasi_hotel;

CREATE TABLE tb_kamar(
    room_id INT(11) NOT NULL PRIMARY KEY,
    tipe_kamar VARCHAR(256) NOT NULL,
    harga_malam INT(11) NOT NULL,
    fasilitas VARCHAR(256) NOT NULL,
    status_kamar VARCHAR(256) NOT NULL
);

# Insert rooms with range id 101-110
INSERT INTO tb_kamar(
    room_id, tipe_kamar, harga_malam, fasilitas, status_kamar
) VALUES
    (101, 'VIP', 1500000 , 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (102, 'Reguler', 150000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (103, 'VIP', 185000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (104, 'Reguler', 150000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (105, 'VVIP', 200000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (106, 'VIP', 185000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (107, 'Reguler', 150000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (108, 'VVIP', 200000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (109, 'VIP', 185000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia'),
    (110, 'Reguler', 150000, 'WIFI, Kamar Mandi, Single Bed', 'Tersedia');

CREATE TABLE tb_pelanggan(
    customer_id INT(11) NOT NULL PRIMARY KEY,
    name VARCHAR(256) NOT NULL,
    email VARCHAR(256) NOT NULL,
    no_telepon VARCHAR(256) NOT NULL,
    alamat VARCHAR(256) NOT NULL
);

INSERT INTO tb_pelanggan(
    customer_id, name, email, no_telepon, alamat
) VALUES
      (201, 'Dean Akmal', 'dean@hotel.id', '628123456789', 'Lemah Abang'),
      (202, 'Bayu Candra', 'bayu@hotel.id', '628123456789', 'Sangkali'),
      (203, 'Akmal', 'akmal@hotel.id', '628123456789', 'Wadas'),
      (204, 'Ammar Farras', 'ammar@hotel.id', '628123456789', 'Peruri'),
      (205, 'Dimas', 'dimas@hotel.id', '628123456789', 'Resinda'),
      (206, 'Dwi Ananda', 'dwiand@hotel.id', '628123456789', 'Bojong'),
      (207, 'Fajar', 'fajar@hotel.id', '628123456789', 'CKM'),
      (208, 'Sulis', 'sulis@hotel.id', '628123456789', 'Anjun'),
      (209, 'Feri', 'feri@hotel.id', '628123456789', 'Bumi'),
      (210, 'Felisha', 'felisha@hotel.id', '628123456789', 'Lamaran');

CREATE TABLE tb_reservasi(
    reservasi_id INT(11) NOT NULL PRIMARY KEY,
    customer_id INT(11) NOT NULL UNIQUE,
    room_id INT(11) NOT NULL UNIQUE,
    jumlah_tamu INT(11) NOT NULL,
    tanggal_checkin DATE NOT NULL DEFAULT NOW(),
    tanggal_checkout DATE,
    status_reservasi VARCHAR(256) NOT NULL,

    FOREIGN KEY (room_id) REFERENCES tb_kamar(room_id) ON DELETE CASCADE ON UPDATE CASCADE ,
    FOREIGN KEY (customer_id) REFERENCES tb_pelanggan(customer_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO tb_reservasi(
    reservasi_id, customer_id, room_id, jumlah_tamu, tanggal_checkin, tanggal_checkout, status_reservasi
) VALUES
    (1, 201, 101, 5, NOW(), '2024-07-29', 'Booked'),
    (2, 202, 102, 4, NOW(), '2024-07-17', 'Booked'),
    (3, 203, 103, 3, NOW(), '2024-07-19', 'Booked'),
    (4, 204, 104, 5, NOW(), '2024-06-10', 'Booked'),
    (5, 205, 105, 8, NOW(), '2024-05-09', 'Booked'),
    (6, 206, 106, 10, NOW(), '2024-07-15', 'Booked'),
    (7, 207, 107, 2, NOW(), '2024-07-18', 'Booked'),
    (8, 208, 108, 3, NOW(), '2024-07-10', 'Booked'),
    (9, 209, 109, 1, NOW(), '2024-07-12', 'Booked'),
    (10, 210, 110, 5, NOW(), '2024-07-13', 'Booked');

CREATE TABLE tb_pembayaran(
    payment_id INT(11) NOT NULL PRIMARY KEY,
    metode_pembayaran VARCHAR(256) NOT NULL,
    jumlah_pembayaran INT(11) NOT NULL,
    tanggal_pembayaran DATE NOT NULL
);

INSERT INTO tb_pembayaran(
    payment_id, metode_pembayaran, jumlah_pembayaran, tanggal_pembayaran
) VALUES
    (001, 'DEBIT', 1500000 , '2024-07-29'),
    (002, 'CASH', 150000, '2024-07-17'),
    (003, 'DEBIT', 185000, '2024-07-19'),
    (004, 'DEBIT', 150000, '2024-06-10'),
    (005, 'CASH', 200000, '2024-05-09'),
    (006, 'CASH', 185000, '2024-07-15'),
    (007, 'DEBIT', 150000, '2024-07-18'),
    (008, 'CASH', 200000, '2024-07-10'),
    (009, 'CASH', 185000, '2024-07-12'),
    (010, 'DEBIT', 150000, '2024-07-13');
