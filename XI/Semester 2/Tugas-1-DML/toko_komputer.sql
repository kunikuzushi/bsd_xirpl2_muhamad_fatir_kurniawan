CREATE DATABASE IF NOT EXISTS toko_komputer;
USE toko_komputer;

CREATE TABLE IF NOT EXISTS items (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    price FLOAT NOT NULL,
    stock INT NOT NULL
);

CREATE TABLE IF NOT EXISTS customer (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    phone_number VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS transaction (
    id INT AUTO_INCREMENT PRIMARY KEY,
    item_id INT,
    customer_id INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (item_id) REFERENCES items(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (customer_id) REFERENCES customer(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO items (name, price, stock) VALUES
('Laptop', 1200.00, 10),
('Desktop PC', 800.00, 15),
('Monitor', 200.00, 20),
('Keyboard', 50.00, 30),
('Mouse', 20.00, 50);

INSERT INTO customer (name, phone_number) VALUES
('Budi Santoso', '621234567890'),
('Siti Rahayu', '629876543210'),
('Ahmad Ibrahim', '625551234567'),
('Dewi Susanti', '623339998888'),
('Joko Prabowo', '627778889999');

INSERT INTO transaction (item_id, customer_id) VALUES
(1, 2),
(2, 4),
(3, 1),
(4, 3),
(5, 5);