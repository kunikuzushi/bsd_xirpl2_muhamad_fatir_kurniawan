CREATE DATABASE db_jubel_online IF NOT EXISTS;

USE db_jubel_online;

CREATE TABLE tb_barang (
    id_barang varchar(5) PRIMARY KEY NOT NULL,
    nama_barang varchar(50) NOT NULL,
    harga float NOT NULL,
    stok int(10) NOT NULL
);

CREATE TABLE tb_pembeli (
    id_pembeli varchar(4) PRIMARY KEY NOT NULL,
    nama_pembeli varchar(50) NOT NULL,
    alamat_pembeli text NOT NULL,
    email_pembeli varchar(30) NOT NULL,
    no_telpn varchar(15) NOT NULL
);

CREATE TABLE tb_penjual (
    id_penjual varchar(3) PRIMARY KEY NOT NULL,
    nama_penjual varchar(50) NOT NULL,
    alamat_penjual text NOT NULL,
    id_barang varchar(5) NOT NULL
);

CREATE TABLE tb_transaksi (
    id_transaksi varchar(3) PRIMARY KEY NOT NULL,
    id_pembeli varchar(4) NOT NULL,
    id_barang varchar(5) NOT NULL,
    jumlah_beli int(20),
    total_harga float NOT NULL,
    id_penjual varchar(3) NOT NULL,
    tgl_transaksi date NOT NULL
);